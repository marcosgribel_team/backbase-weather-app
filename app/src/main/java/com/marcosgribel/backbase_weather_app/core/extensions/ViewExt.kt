package com.marcosgribel.backbase_weather_app.core.extensions

import android.view.View
import com.google.android.material.snackbar.Snackbar


fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.showSnackbar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}