package com.marcosgribel.backbase_weather_app.core.network

import com.marcosgribel.backbase_weather_app.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class OkHttpClientConfig(private val interceptors: Array<Interceptor>? = null) {

    private val httpLoggingInterceptor: HttpLoggingInterceptor by lazy(LazyThreadSafetyMode.NONE) {
        HttpLoggingInterceptor()
            .apply {
                level = if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor.Level.BODY
                } else {
                    HttpLoggingInterceptor.Level.NONE
                }
            }
    }

    val config: OkHttpClient = OkHttpClient.Builder()
        .apply {
            addInterceptor(httpLoggingInterceptor)
            interceptors?.map { interceptor ->
                addInterceptor(interceptor)
            }
        }
        .build()


}