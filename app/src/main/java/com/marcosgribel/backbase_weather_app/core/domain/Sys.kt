package com.marcosgribel.backbase_weather_app.core.domain

import com.google.gson.annotations.SerializedName


data class Sys(
    @SerializedName("country") val country: String
)