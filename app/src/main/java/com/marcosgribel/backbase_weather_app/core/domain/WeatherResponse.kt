package com.marcosgribel.backbase_weather_app.core.domain

import com.google.gson.annotations.SerializedName

data class WeatherResponse (
	@SerializedName("message") val message : String,
	@SerializedName("cod") val cod : Int,
	@SerializedName("count") val count : Int,
	@SerializedName("list") val list : List<WeatherList>
)