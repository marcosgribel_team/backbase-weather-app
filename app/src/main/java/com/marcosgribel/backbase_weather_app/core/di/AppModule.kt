package com.marcosgribel.backbase_weather_app.core.di

import android.content.Context
import com.marcosgribel.backbase_weather_app.BuildConfig
import com.marcosgribel.backbase_weather_app.core.network.OkHttpClientConfig
import com.marcosgribel.backbase_weather_app.core.network.RetrofitProvider
import com.marcosgribel.backbase_weather_app.core.network.api.WeatherApi
import com.marcosgribel.backbase_weather_app.core.network.interceptor.WeatherApiKeyInterceptor
import com.marcosgribel.backbase_weather_app.feature.weather.bookmark.viewmodel.WeatherBookmarkFragmentViewModel
import com.marcosgribel.backbase_weather_app.feature.weather.search.viewmodel.WeatherSearchFragmentViewModel
import com.marcosgribel.backbase_weather_app.model.WeatherRepository
import com.marcosgribel.backbase_weather_app.model.storage.LocalStorage
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val networkModule = module {
    single { OkHttpClientConfig(arrayOf(WeatherApiKeyInterceptor())).config }
    single { RetrofitProvider(BuildConfig.WEATHER_API_URL, get()) }
}


val modelModule = module {
    single {
        val preferences = androidContext().getSharedPreferences(
            LocalStorage::class.java.simpleName,
            Context.MODE_PRIVATE
        )
        LocalStorage(preferences)
    }
    factory { WeatherRepository(get<RetrofitProvider>().create(WeatherApi::class.java), get()) }
}

val bookmarkModule = module {
    viewModel { WeatherBookmarkFragmentViewModel(get()) }
}

val searchModule = module {
    viewModel { WeatherSearchFragmentViewModel(get()) }
}

val appModules = listOf(networkModule, modelModule, bookmarkModule, searchModule)