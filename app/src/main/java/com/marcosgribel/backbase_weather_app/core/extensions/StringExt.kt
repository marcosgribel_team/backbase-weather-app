package com.marcosgribel.backbase_weather_app.core.extensions

import com.google.gson.GsonBuilder


fun <T> String.fromJson(clazz: Class<T>): T {
    return GsonBuilder().create().fromJson(this, clazz)
}