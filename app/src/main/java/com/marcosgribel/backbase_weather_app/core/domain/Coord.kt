package com.marcosgribel.backbase_weather_app.core.domain

import com.google.gson.annotations.SerializedName

data class Coord (
	@SerializedName("lat") val lat : Double,
	@SerializedName("lon") val lon : Double
)