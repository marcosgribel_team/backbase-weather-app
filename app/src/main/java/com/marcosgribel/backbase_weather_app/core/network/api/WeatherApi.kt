package com.marcosgribel.backbase_weather_app.core.network.api

import com.marcosgribel.backbase_weather_app.core.domain.WeatherResponse
import com.marcosgribel.backbase_weather_app.core.domain.enumerator.WeatherUnitEnum
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("find")
    suspend fun findByCity(
        @Query("q") query: String,
        @Query("units") unit: WeatherUnitEnum? = WeatherUnitEnum.metric
    ): WeatherResponse

    @GET("group")
    suspend fun findById(
        @Query("id") ids: String?,
        @Query("units") unit: WeatherUnitEnum? = WeatherUnitEnum.metric
    ): WeatherResponse
}