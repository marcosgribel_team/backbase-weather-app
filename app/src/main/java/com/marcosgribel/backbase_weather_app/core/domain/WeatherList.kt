package com.marcosgribel.backbase_weather_app.core.domain

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName

data class WeatherList(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("coord") val coord: Coord?,
    @SerializedName("main") val main: Main?,
    @SerializedName("dt") val dt: Int?,
    @SerializedName("wind") val wind: Wind?,
    @SerializedName("sys") val sys: Sys?,
    @SerializedName("clouds") val clouds: Clouds?,
    @SerializedName("weather") val weather: List<Weather>?
) {

    fun toJson(): String {
        return GsonBuilder().create().toJson(this)
    }

}