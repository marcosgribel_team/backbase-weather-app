package com.marcosgribel.backbase_weather_app.core.extensions

import androidx.appcompat.widget.SearchView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.launch

fun SearchView.onSearchTextChange(
    lifecycleScope: CoroutineScope
): ConflatedBroadcastChannel<String> {
    val queryChannel = ConflatedBroadcastChannel<String>()

    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            return true
        }

        override fun onQueryTextChange(query: String): Boolean {
            lifecycleScope.launch {
                queryChannel.send(query)
            }
            return true
        }
    })
    return queryChannel
}