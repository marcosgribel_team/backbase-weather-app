package com.marcosgribel.backbase_weather_app.core.network.interceptor

import com.marcosgribel.backbase_weather_app.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class WeatherApiKeyInterceptor : Interceptor {

    companion object {
        private const val QUERY_PARAM_APP_ID = "appid"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newUrl = request.url.newBuilder().addQueryParameter(
            QUERY_PARAM_APP_ID,
            BuildConfig.WEATHER_APP_ID
        ).build()

        val newRequest = request.newBuilder().url(newUrl).build()
        return chain.proceed(newRequest)
    }
}