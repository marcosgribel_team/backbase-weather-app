package com.marcosgribel.backbase_weather_app.core.domain.enumerator

enum class WeatherUnitEnum {
    imperial, // Fahrenheit
    metric // Celsius
}