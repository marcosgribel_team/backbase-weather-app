package com.marcosgribel.backbase_weather_app.core.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitProvider constructor(
    private val baseUrl: String,
    private val okHttpClient: OkHttpClient
) {

    private val retrofit: Retrofit by lazy(LazyThreadSafetyMode.NONE) {

        Retrofit.Builder().apply {
            baseUrl(baseUrl)
            client(okHttpClient)
            addConverterFactory(GsonConverterFactory.create())
        }.build()
    }

    fun <T> create(clazz: Class<T>): T = retrofit.create(clazz)
}