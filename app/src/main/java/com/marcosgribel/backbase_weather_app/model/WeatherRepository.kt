package com.marcosgribel.backbase_weather_app.model

import com.marcosgribel.backbase_weather_app.core.domain.WeatherList
import com.marcosgribel.backbase_weather_app.core.domain.WeatherResponse
import com.marcosgribel.backbase_weather_app.core.network.api.WeatherApi
import com.marcosgribel.backbase_weather_app.model.storage.LocalStorage

class WeatherRepository(
    private val weatherApi: WeatherApi,
    private val localStorage: LocalStorage
) {

    companion object {
        private const val KEY_WEATHER_BOOKMARK = "KEY_WEATHER_BOOKMARK"
        private const val SEPARATOR_COMMA = ","
    }

    suspend fun findByName(query: String): WeatherResponse = weatherApi.findByCity(query)

    suspend fun fetchBookmarks(): WeatherResponse? {
        val ids = getBookmarks() ?: emptySet()
        if(ids.isNullOrEmpty()) {
            return null
        }
        return weatherApi.findById(ids?.joinToString(SEPARATOR_COMMA))
    }

    fun addBookmarks(item: WeatherList) {
        val ids = getBookmarks()?.toMutableSet()
        ids?.add(item.id)
        localStorage.save(KEY_WEATHER_BOOKMARK, ids?.joinToString(SEPARATOR_COMMA))
    }

    fun removeBookmarks(id: Long) {
        val ids = getBookmarks()?.toMutableSet()
        ids?.remove(id)
        localStorage.save(KEY_WEATHER_BOOKMARK, ids?.joinToString(SEPARATOR_COMMA))
    }

    private fun getBookmarks(): Set<Long>? {
        val bookmarks = localStorage.get(KEY_WEATHER_BOOKMARK)
        return bookmarks?.split(SEPARATOR_COMMA)
            ?.filter { it.isNotEmpty() }
            ?.map {
                it.toLong()
            }?.toSet()
    }

}