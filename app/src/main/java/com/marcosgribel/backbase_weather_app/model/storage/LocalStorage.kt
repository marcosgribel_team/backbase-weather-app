package com.marcosgribel.backbase_weather_app.model.storage

import android.content.SharedPreferences

class LocalStorage(private val sharedPreferences: SharedPreferences) {

    fun save(key: String, value: String?) {
        sharedPreferences.edit()
            .putString(key, value)
            .apply()
    }

    fun get(key: String): String? {
        return sharedPreferences.getString(key, "")
    }

}