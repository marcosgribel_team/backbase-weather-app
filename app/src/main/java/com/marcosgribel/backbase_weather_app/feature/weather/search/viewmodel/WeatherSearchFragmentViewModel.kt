package com.marcosgribel.backbase_weather_app.feature.weather.search.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.marcosgribel.backbase_weather_app.core.domain.WeatherList
import com.marcosgribel.backbase_weather_app.model.WeatherRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


sealed class SearchViewModeState<out RESULT> {
    object Loading : SearchViewModeState<Nothing>()
    object Empty : SearchViewModeState<Nothing>()
    class Success<out RESULT>(val result: RESULT) : SearchViewModeState<RESULT>()
    class Failure(val throwable: Throwable) : SearchViewModeState<Nothing>()
}

class WeatherSearchFragmentViewModel(private val repository: WeatherRepository) : ViewModel() {

    private val _weatherListByCityLiveData =
        MutableLiveData<SearchViewModeState<List<WeatherList>>>()
    val weatherListByCityLiveData: LiveData<SearchViewModeState<List<WeatherList>>> =
        _weatherListByCityLiveData


    fun addSearchQueryChangedListener(queryChannel: ConflatedBroadcastChannel<String>) {
        viewModelScope.launch {
            queryChannel
                .asFlow()
                .debounce(500)
                .filter { it.isNotEmpty() }
                .distinctUntilChanged()
                .collect { search(it) }
        }
    }

    private fun search(query: String) {
        _weatherListByCityLiveData.postValue(SearchViewModeState.Loading)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.findByName(query)
                    if (response.list.isNullOrEmpty()) {
                        _weatherListByCityLiveData.postValue(SearchViewModeState.Empty)
                    } else {
                        _weatherListByCityLiveData.postValue(SearchViewModeState.Success(response.list))
                    }
                } catch (t: Throwable) {
                    _weatherListByCityLiveData.postValue(SearchViewModeState.Failure(t))
                }
            }
        }
    }

    fun addWeatherBookmark(item: WeatherList) {
        repository.addBookmarks(item)
    }
}