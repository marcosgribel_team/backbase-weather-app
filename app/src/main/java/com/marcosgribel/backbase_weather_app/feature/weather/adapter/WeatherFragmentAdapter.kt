package com.marcosgribel.backbase_weather_app.feature.weather.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.backbase_weather_app.R
import com.marcosgribel.backbase_weather_app.core.domain.WeatherList
import kotlinx.android.synthetic.main.item_weather_search.view.*

internal class WeatherFragmentAdapter(private val onItemClick: (item: WeatherList) -> Unit) :
    RecyclerView.Adapter<WeatherFragmentAdapter.ViewHolder>() {

    private val items = mutableListOf<WeatherList>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_weather_search,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.onBind(items[position], onItemClick)

    override fun getItemId(position: Int): Long = items[position].id.toLong()

    fun addAll(items: List<WeatherList>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun remove(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    internal class ViewHolder(
        private val view: View
    ) : RecyclerView.ViewHolder(view) {

        private val weatherTypes = hashMapOf(
            "clouds" to R.raw.weather_windy,
            "clear" to R.raw.weather_sunny,
            "snow" to R.raw.weather_snow,
            "rain" to R.raw.weather_partly_shower
        )


        fun onBind(item: WeatherList, onItemClick: (item: WeatherList) -> Unit) {
            val weather = item.weather?.get(0)
            val weatherAnimation = weatherTypes[weather?.main?.toLowerCase()]
            weatherAnimation?.let {
                view.iv_item_weather_search_icon.setAnimation(it)
            }

            view.tv_item_search_city_name.text = "${item.name}, ${item.sys?.country}"
            view.tv_weather_description.text = weather?.description
            view.tv_item_search_search_temperature.text =
                view.context.getString(
                    R.string.format_temperature,
                    item.main?.temp?.toInt().toString()
                )
            view.setOnClickListener { onItemClick(item) }
        }
    }
}