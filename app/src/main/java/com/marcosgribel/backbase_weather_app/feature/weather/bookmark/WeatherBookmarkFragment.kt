package com.marcosgribel.backbase_weather_app.feature.weather.bookmark


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.backbase_weather_app.R
import com.marcosgribel.backbase_weather_app.core.domain.WeatherList
import com.marcosgribel.backbase_weather_app.core.extensions.showSnackbar
import com.marcosgribel.backbase_weather_app.feature.weather.adapter.WeatherFragmentAdapter
import com.marcosgribel.backbase_weather_app.feature.weather.bookmark.viewmodel.WeatherBookmarkFragmentViewModel
import com.marcosgribel.backbase_weather_app.feature.weather.detail.WeatherDetailFragment.Companion.BUNDLE_PARAM_WEATHER
import com.marcosgribel.backbase_weather_app.feature.weather.search.viewmodel.SearchViewModeState
import kotlinx.android.synthetic.main.fragment_weather_bookmark.*
import kotlinx.android.synthetic.main.fragment_weather_search.*
import kotlinx.android.synthetic.main.layout_weather_result.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class WeatherBookmarkFragment : Fragment() {

    private val viewModel by viewModel<WeatherBookmarkFragmentViewModel>()
    private lateinit var weatherAdapter: WeatherFragmentAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather_bookmark, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.setOnClickListener { onBtnSearchClicked() }

        // RecyclerView configuration
        val linearLayoutManager = LinearLayoutManager(context)
        val dividerItemDecoration = DividerItemDecoration(
            context,
            linearLayoutManager.orientation
        )

        weatherAdapter = WeatherFragmentAdapter { onItemClicked(it) }
        view.rv_weather_search.apply {
            adapter = weatherAdapter
            layoutManager = linearLayoutManager
            addItemDecoration(dividerItemDecoration)

        }

        viewModel.fetch()
        viewModel.weatherListByCityLiveData.observe(this, Observer {
            when (it) {
                is SearchViewModeState.Loading -> onLoading()
                is SearchViewModeState.Empty -> onEmpty()
                is SearchViewModeState.Success -> onSuccess(it.result)
                is SearchViewModeState.Failure -> onFailure(it.throwable)
            }
        })

        val itemTouchHelper = ItemTouchHelper(onItemTouchCallback())
        itemTouchHelper.attachToRecyclerView(rv_weather_search)
    }


    private fun onLoading() {
        // Do nothing
    }

    private fun onEmpty() {
        // Do nothing
    }

    private fun onSuccess(items: List<WeatherList>) {
        weatherAdapter.addAll(items)
    }

    private fun onFailure(throwable: Throwable) {
        view?.showSnackbar(getString(R.string.msg_error_something_went_wrong))
    }

    /**
     * This method handle the callback when the item is swiped to left and trigger the deletion
     */
    private fun onItemTouchCallback(): ItemTouchHelper.SimpleCallback {
        return object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val id = weatherAdapter.getItemId(viewHolder.adapterPosition)
                weatherAdapter.remove(viewHolder.adapterPosition)
                viewModel.deleteBookmark(id)
            }
        }
    }

    private fun onBtnSearchClicked() {
        WeatherBookmarkFragmentDirections.actionWeatherBookmarkFragmentToWeatherSearchFragment()
            .run {
                view?.findNavController()?.navigate(this)
            }
    }

    private fun onItemClicked(item: WeatherList) {
        val bundle = bundleOf(BUNDLE_PARAM_WEATHER to item.toJson())
        view?.findNavController()
            ?.navigate(R.id.action_weatherBookmarkFragment_to_weatherDetailFragment, bundle)
    }
}
