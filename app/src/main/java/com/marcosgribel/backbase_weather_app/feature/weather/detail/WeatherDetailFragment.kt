package com.marcosgribel.backbase_weather_app.feature.weather.detail


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.marcosgribel.backbase_weather_app.R
import com.marcosgribel.backbase_weather_app.core.domain.WeatherList
import com.marcosgribel.backbase_weather_app.core.extensions.fromJson
import kotlinx.android.synthetic.main.fragment_weather_detail.view.*

/**
 * A simple [Fragment] subclass.
 */
class WeatherDetailFragment : Fragment() {


    companion object {
        const val BUNDLE_PARAM_WEATHER = "BUNDLE_PARAM_WEATHER"
    }

    private val weatherTypes = hashMapOf(
        "clouds" to R.drawable.cloud,
        "clear" to R.drawable.sun,
        "snow" to R.drawable.snow,
        "rain" to R.drawable.rain
    )


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val weatherList = arguments?.getString(BUNDLE_PARAM_WEATHER)?.fromJson(WeatherList::class.java)

        val weather = weatherList?.weather?.get(0)
        val weatherAnimation = weatherTypes[weather?.main?.toLowerCase()]
        weatherAnimation?.let {
            view.iv_weather_detail_background.setImageResource(it)
        }

        view.tv_weather_detail_temperature.text =
            getString(
                R.string.format_temperature,
                weatherList?.main?.temp?.toInt().toString()
            )
        view.tv_weather_detail_city_name.text = "${weatherList?.name}, ${weatherList?.sys?.country}"

        view.tv_weather_detail_humidity_value.text = weatherList?.main?.humidity.toString()
        view.tv_weather_detail_wind_value.text = weatherList?.wind?.speed.toString()
    }

}
