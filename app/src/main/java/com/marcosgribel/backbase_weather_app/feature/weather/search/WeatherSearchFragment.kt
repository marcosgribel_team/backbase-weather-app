package com.marcosgribel.backbase_weather_app.feature.weather.search


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.marcosgribel.backbase_weather_app.R
import com.marcosgribel.backbase_weather_app.core.domain.WeatherList
import com.marcosgribel.backbase_weather_app.core.extensions.gone
import com.marcosgribel.backbase_weather_app.core.extensions.onSearchTextChange
import com.marcosgribel.backbase_weather_app.core.extensions.visible
import com.marcosgribel.backbase_weather_app.feature.weather.adapter.WeatherFragmentAdapter
import com.marcosgribel.backbase_weather_app.feature.weather.search.viewmodel.SearchViewModeState
import com.marcosgribel.backbase_weather_app.feature.weather.search.viewmodel.WeatherSearchFragmentViewModel
import kotlinx.android.synthetic.main.fragment_weather_search.*
import kotlinx.android.synthetic.main.fragment_weather_search.view.*
import org.koin.android.viewmodel.ext.android.viewModel


class WeatherSearchFragment : Fragment() {

    private val viewModel by viewModel<WeatherSearchFragmentViewModel>()
    private lateinit var searchAdapter: WeatherFragmentAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_weather_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // RecyclerView configuration
        val linearLayoutManager = LinearLayoutManager(context)
        val dividerItemDecoration = DividerItemDecoration(
            context,
            linearLayoutManager.orientation
        )

        searchAdapter = WeatherFragmentAdapter { onItemClickListener(it) }
        view.rv_weather_search.apply {
            adapter = searchAdapter
            layoutManager = linearLayoutManager
            addItemDecoration(dividerItemDecoration)
        }

        viewModel.addSearchQueryChangedListener(
            sv_weather_search_query.onSearchTextChange(
                lifecycleScope
            )
        )

        viewModel.weatherListByCityLiveData.observe(this, Observer {
            when (it) {
                is SearchViewModeState.Loading -> onLoading()
                is SearchViewModeState.Empty -> onEmpty()
                is SearchViewModeState.Success -> onSuccess(it.result)
                is SearchViewModeState.Failure -> onFailure(it.throwable)
            }
        })
    }

    private fun onItemClickListener(item: WeatherList) {
        viewModel.addWeatherBookmark(item)
        view?.findNavController()?.navigateUp()
    }

    private fun onLoading() {
        search_progress_bar.visible()
        rv_weather_search.gone()
        view_weather_search_empty.gone()
    }

    private fun onEmpty() {
        search_progress_bar.gone()
        rv_weather_search.gone()
        view_weather_search_empty.visible()
    }

    private fun onSuccess(item: List<WeatherList>) {
        search_progress_bar.gone()
        rv_weather_search.visible()
        view_weather_search_empty.gone()
        searchAdapter.addAll(item)
    }

    private fun onFailure(throwable: Throwable) {
        activity?.findViewById<View>(R.id.nav_host_fragment)
            ?.let {
                Snackbar.make(it, getString(R.string.msg_error_something_went_wrong), Snackbar.LENGTH_LONG).show()
            }
    }
}
