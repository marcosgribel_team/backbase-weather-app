package com.marcosgribel.backbase_weather_app.feature.weather.bookmark.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.marcosgribel.backbase_weather_app.core.domain.WeatherList
import com.marcosgribel.backbase_weather_app.feature.weather.search.viewmodel.SearchViewModeState
import com.marcosgribel.backbase_weather_app.model.WeatherRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class WeatherBookmarkFragmentViewModel(private val repository: WeatherRepository) : ViewModel() {

    private val _weatherListByCityLiveData =
        MutableLiveData<SearchViewModeState<List<WeatherList>>>()
    val weatherListByCityLiveData: LiveData<SearchViewModeState<List<WeatherList>>> =
        _weatherListByCityLiveData

    fun fetch() {
        _weatherListByCityLiveData.postValue(SearchViewModeState.Loading)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val response = repository.fetchBookmarks()
                    if(response == null || response.list.isNullOrEmpty()) {
                        _weatherListByCityLiveData.postValue(SearchViewModeState.Empty)
                    } else {
                        _weatherListByCityLiveData.postValue(SearchViewModeState.Success(response.list))
                    }
                } catch (t: Throwable) {
                    _weatherListByCityLiveData.postValue(SearchViewModeState.Failure(t))
                }
            }
        }
    }

    fun deleteBookmark(id: Long) {
        repository.removeBookmarks(id)
    }
}