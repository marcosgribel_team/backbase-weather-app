package com.marcosgribel.backbase_weather_app

import android.app.Application
import com.marcosgribel.backbase_weather_app.core.di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(applicationContext)
            modules(appModules)
        }
    }
}