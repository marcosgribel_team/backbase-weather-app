package com.marcosgribel.backbase_weather_app.core.network.interceptor

import com.marcosgribel.backbase_weather_app.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class WeatherApiKeyInterceptorTest {

    private val mockWebServer = MockWebServer()
    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(WeatherApiKeyInterceptor())
        .build()

    @Before
    fun setUp() {
        mockWebServer.start()

    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }


    @Test
    fun `test interceptor add APP_ID as query parameter`() {
        val mockResponse = MockResponse()
        mockWebServer.enqueue(mockResponse)

        val request = Request.Builder()
            .url(mockWebServer.url("/"))
            .build()

        val response = okHttpClient.newCall(request).execute()
        Assert.assertNotNull(response)
        Assert.assertTrue(response.request.url.toString().contains("?appid=${BuildConfig.WEATHER_APP_ID}"))
    }
}