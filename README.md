# Weather Appgit 

### Get Started ###

* Download [Android Studio Preview](https://developer.android.com/studio/preview/)
* Checkout the Project `git clone git clone https://marcosgribel@bitbucket.org/marcosgribel_team/backbase-weather-app.git`


## The Project

This project leverages on several modern approaches to build Android applications :

- 100% written in Kotlin.
- [MVVM Architecture](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel). powered by Android JetPack and Coroutines Flow
- Easy code modularization 
    - All packages is organized following the pattern "package by feature", that way is easily to modularize the project when it grows. 
    Also, in case the teams are divided by features you can easily modularize it by team/feature.
- Based on [Material Design](https://material.io/design/) guideline.
- Clean Code practices
- SOLID Code practices
- Unit Tests

### Libraries
- Android Jetpack
- Coroutines
- Koin
- Retrofit
- Picasso
- Lottie

### [Screenshots](https://bitbucket.org/marcosgribel_team/backbase-weather-app/src/master/screenshots/)

